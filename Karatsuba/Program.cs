﻿using System.Numerics;

public class KaratsubaMultiplication
{
    public static string Multiply(string num1, string num2)
    {
        int n = Math.Max(num1.Length, num2.Length);

        if (n == 1)
        {
            return (int.Parse(num1) * int.Parse(num2)).ToString();
        }
        else if(n == 2)
        {
            return (int.Parse(num1) * int.Parse(num2)).ToString();
        }

        int n2 = n / 2;

        string a = num1.Substring(0, num1.Length - n2);
        string b = num1.Substring(num1.Length - n2);
        string c = num2.Substring(0, num2.Length - n2);
        string d = num2.Substring(num2.Length - n2);

        string ac = Multiply(a, c);
        string bd = Multiply(b, d);

        string adbc = Multiply(Add(a, b), Add(c, d));
        string adbcMinusACMinusBD = Subtract(adbc, Add(ac, bd));

        return Add(Add(PadRight(ac, 2 * n2), PadRight(adbcMinusACMinusBD, n2)), bd);
    }


    private static string Add(string num1, string num2)
    {
        int carry = 0;
        int n = Math.Max(num1.Length, num2.Length);
        char[] result = new char[n];
        int i = num1.Length - 1, j = num2.Length - 1, k = n - 1;

        while (i >= 0 || j >= 0)
        {
            int digit1 = (i >= 0) ? int.Parse(num1[i].ToString()) : 0;
            int digit2 = (j >= 0) ? int.Parse(num2[j].ToString()) : 0;

            int sum = carry + digit1 + digit2;
            carry = sum / 10;
            result[k--] = (char)((sum % 10) + '0');

            i--;
            j--;
        }

        if (carry > 0)
            return "1" + new string(result);
        else
            return new string(result);
    }

    private static string Subtract(string num1, string num2)
    {
        int borrow = 0;
        int n = Math.Max(num1.Length, num2.Length);
        char[] result = new char[n];
        int i = num1.Length - 1, j = num2.Length - 1, k = n - 1;

        while (i >= 0 || j >= 0)
        {
            int digit1 = (i >= 0) ? int.Parse(num1[i].ToString()) : 0;
            int digit2 = (j >= 0) ? int.Parse(num2[j].ToString()) : 0;

            int diff = digit1 - digit2 - borrow;

            if (diff < 0)
            {
                diff += 10;
                borrow = 1;
            }
            else
            {
                borrow = 0;
            }

            result[k--] = (char)(diff + '0');

            i--;
            j--;
        }

        // Remove leading zeros
        int startIndex = 0;
        while (startIndex < n - 1 && result[startIndex] == '0')
        {
            startIndex++;
        }

        return new string(result, startIndex, n - startIndex);
    }


    private static string PadRight(string num, int count)
    {
        return num + new string('0', count);
    }

    public static void Main(string[] args)
    {
        string num1 = "123456789101112131415";
        string num2 = "151413121110987654321";

        string result = Multiply(num1, num2);

        Console.WriteLine($"{num1} * {num2} = {result}");

        if(result == "18692977760140351820842129773147544594215")
        {
            Console.WriteLine("AHMADOU YOU ARE THE BEST");
        }
        Console.ReadLine();
    }
}
