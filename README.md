# Karatsuba

L'algorithme de Karatsuba est un algorithme pour multiplier rapidement deux nombres de n chiffres avec une complexité temporelle en O($n^{log2(3)}$) ≈ O($n^{1,585}$) au lieu de O($n^2$) pour la méthode naïve. Il a été développé par Anatolii Alexevich Karatsuba en 1960 et publié en 1962. 

## Principe

Pour multiplier deux nombres de n chiffres, la méthode naïve multiplie chaque chiffre du multiplicateur par chaque chiffre du multiplicande. Cela exige donc $n^2$ produits de deux chiffres. Le temps de calcul est en O($n^2$).

En 1960, Karatsuba remarque que pour tout k, le calcul naïf d'un produit : 
    (a * $10^k$ + b) * (c * $10^k$ + d) = ac * $10^{2k}$ + (ad + bc) * $10^k$ + bd

qui semble nécessiter les quatre produits ac, ad, bc et bd, peut en fait être effectué seulement avec les trois produits ac, bd et (a – b)(c – d) en regroupant les calculs sous la forme suivante :
    (a * $10^k$ + b) * (c * $10^k$ + d) = ac * $10^{2k}$ + (ac + bd - (a - b)(c - d)) * $10^k$ + bd

    
Pour de grands nombres et en prenant k = $^n/_2$, la méthode peut être appliquée de manière récursive pour les calculs de ac, bd et (a – b)(c – d) en scindant à nouveau a, b, c et d en deux et ainsi de suite. C'est un algorithme de type diviser pour régner.


La multiplication par la base de numération (10 dans l'exemple précédent, mais 2 pour les machines) correspond à un décalage de chiffre, et les additions sont peu coûteuses en temps ; ainsi, le fait d'être capable de calculer les grandeurs nécessaires en 3 produits au lieu de 4 mène à une amélioration de complexité

## Exemple

Exécutons l'algorithme pour calculer le produit 1237 × 2587.

a = 12, b = 37, c = 25 et d = 87
- étape 1 (e1) : a * c = 300
- étape 2 (e2) : b * d = 3219
- étape 3 (e3) : (a + b) * (c + d) = 5488
- étape 4 (e4) : e3 - e2 - e1 = 1969
- étape 5 (e5) : e1 * 10000 + e4 * 100 + e2 =  3000000 + 196900 + 3219 = 3200119.
